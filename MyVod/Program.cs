﻿using System;
using MyVod.Domain.Movies;
using MyVod.Infrastructure.Movies;

namespace MyVod
{
    class Program
    {
        static void Main(string[] args)
        {
            // using var moviesContext = new MoviesContext();
            //
            // #region DropAndCreateDatabase
            // moviesContext.Database.EnsureDeleted();
            // moviesContext.Database.EnsureCreated();
            // #endregion
            //
            // var movie = new Movie("Die Hard", "Yippee Ki Yay");
            // var bruce = new CastMember("Bruce", "Willis");
            //
            // movie.Apply(new Movie.Events.AddCastMemberEvent
            // {
            //     CastMember = bruce
            // });
            //
            // foreach (var castMember in movie.Cast)
            // {
            //     Console.WriteLine($"{castMember.FirstName} {castMember.LastName}");
            // }
            //
            // moviesContext.Movies.Add(movie);
            // moviesContext.SaveChanges();
        }
    }
}