using MediatR;

namespace MyVod.Domain.Movies.Events;

public record MoviePublished(Movie Movie) : INotification;