using MediatR;

namespace MyVod.Domain.Movies.Events;

public record MovieCreated(Movie Movie) : INotification;