using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Linecode.DDD;
using Linecode.DDD.Exceptions;
using MediatR;
using MyVod.Domain.Movies.Commands;
using MyVod.Domain.Movies.Repositories;

namespace MyVod.Domain.Movies.Handlers;

internal class GetMovieHandler : IRequestHandler<GetMovieCommand, Result<Movie>>
{
    private readonly IMoviesRepository _repository;
    private readonly IEventStore _eventStore;

    public GetMovieHandler(IMoviesRepository repository, IEventStore eventStore)
    {
        _repository = repository;
        _eventStore = eventStore;
    }
        
    public async Task<Result<Movie>> Handle(GetMovieCommand request, CancellationToken cancellationToken)
    {
        var movie = await _repository.Get(request.MovieId);
        
        if (movie is null)
            return Result<Movie>.Error(new NotFoundException());
        
        return Result<Movie>.Success(movie);
        //
        // var movie = await _eventStore.Get(request.MovieId.Value);
        //     
        // if (movie is null)
        //     return Result<Movie>.Error(new NotFoundException());
        //
        // return Result<Movie>.Success(movie);
    }
}