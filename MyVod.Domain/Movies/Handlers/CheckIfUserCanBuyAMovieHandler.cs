using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Linecode.DDD;
using Linecode.DDD.Exceptions;
using MediatR;
using MyVod.Domain.Movies.Commands;
using MyVod.Domain.Movies.Policies.Buy;
using MyVod.Domain.Movies.Repositories;
using MyVod.Domain.Shared;

namespace MyVod.Domain.Movies.Handlers;

internal class CheckIfUserCanBuyAMovieHandler : IRequestHandler<CheckIfUserCanBuyAMovieCommand, Result<bool>>
{
    private readonly IMoviesRepository _repository;

    public CheckIfUserCanBuyAMovieHandler(IMoviesRepository repository)
    {
        _repository = repository;
    }
        
    public async Task<Result<bool>> Handle(CheckIfUserCanBuyAMovieCommand request, CancellationToken cancellationToken)
    {
        var movie = await _repository.Get(request.MovieId);
            
        if (movie is null)
            return Result<bool>.Error(new NotFoundException());

        #region hidden
        // var policy = BuyPolicyStrategy.GetPolicy(request.User);
        #endregion

        BuyPolicy policy;

        if (request.User.Groups.Contains("Vip"))
            policy = BuyPolicies.Vip;
        else
            policy = BuyPolicies.Default;

        var result = movie.CanUserBuy(request.User, policy);
            
        return Result<bool>.Success(result.IsSuccessful);
    }
}

#region refactor
    
internal static class BuyPolicyStrategy
{
    internal static BuyPolicy GetPolicy(User user)
        => user.Groups.Contains("Vip") ? BuyPolicies.Vip : BuyPolicies.Default;
}
    
#endregion