using System;
using System.Threading;
using System.Threading.Tasks;
using Linecode.DDD;
using MediatR;
using MyVod.Domain.Movies.Commands;
using MyVod.Domain.Movies.Repositories;

namespace MyVod.Domain.Movies.Handlers
{
    internal class CreateMovieHandler : IRequestHandler<CreateMovieCommand, Result<MovieId>>
    {
        private readonly IMoviesRepository _moviesRepository;
        private readonly IEventStore _eventStore;

        public CreateMovieHandler(IMoviesRepository moviesRepository, IEventStore eventStore)
        {
            _moviesRepository = moviesRepository;
            _eventStore = eventStore;
        }
        
        public async Task<Result<MovieId>> Handle(CreateMovieCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var movie = new Movie(request.Title, request.Description);
                // var movie = new Movie();
                // var @event = new Movie.Events.CreateMovie(request.Title, request.Description);
                
                // movie.Apply(@event);
                
                _moviesRepository.Add(movie);
                await _moviesRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);

                // await _eventStore.Add(movie.Id.Value, @event, movie.Version);
                return Result<MovieId>.Success(movie.Id);
            }
            catch (Exception e)
            {
                return Result<MovieId>.Error(e);
            }
        }
    }
}