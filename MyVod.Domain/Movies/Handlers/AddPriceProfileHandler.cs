using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Linecode.DDD;
using Linecode.DDD.Exceptions;
using MediatR;
using MyVod.Domain.Movies.Commands;
using MyVod.Domain.Movies.Repositories;

namespace MyVod.Domain.Movies.Handlers;

internal class AddPriceProfileHandler : IRequestHandler<AddPriceProfileCommand, Result>
{
    private readonly IMoviesRepository _repository;
    private readonly IEventStore _eventStore;

    public AddPriceProfileHandler(IMoviesRepository repository, IEventStore eventStore)
    {
        _repository = repository;
        _eventStore = eventStore;
    }
        
    public async Task<Result> Handle(AddPriceProfileCommand request, CancellationToken cancellationToken)
    {
        var movie = await _repository.Get(request.MovieId);
        // var movie = await _eventStore.Get(request.MovieId.Value);

        var pricingProfile = PricingProfile.GetBasedOnCode(request.PricingProfileCode);
        var newEvent = new Movie.Events.AddPricingProfile(pricingProfile);
            
        movie.Apply(newEvent);

        // await _eventStore.Add(movie.Id.Value, newEvent, movie.Version);
        
        _repository.Update(movie);
        await _repository.UnitOfWork.SaveEntitiesAsync(cancellationToken);

        return Result.Success();
    }
}