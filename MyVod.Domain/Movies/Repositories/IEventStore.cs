using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Linecode.DDD;

namespace MyVod.Domain.Movies.Repositories
{
    public interface IEventStore
    {
        Task Add(Guid aggregateId, object eventPayload, int version);
        Task<Movie> Get(Guid aggregateId);
    }
}