using Linecode.DDD;
using MyVod.Domain.Shared;

namespace MyVod.Domain.Movies.Policies.Buy
{
    public interface BuyPolicy
    {
        Result Buy(Movie movie, User user);
    }
}