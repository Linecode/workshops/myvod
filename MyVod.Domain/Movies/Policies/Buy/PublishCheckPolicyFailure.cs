using System;

namespace MyVod.Domain.Movies.Policies.Buy
{
    public class PublishCheckPolicyFailure : Exception {}
}