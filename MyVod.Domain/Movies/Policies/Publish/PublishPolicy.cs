using Linecode.DDD;

namespace MyVod.Domain.Movies.Policies.Publish
{
    public interface PublishPolicy
    {
        Result Publish(Movie movie);
    }
}