using System.Linq;
using Linecode.DDD;

namespace MyVod.Domain.Movies.Policies.Publish
{
    public static class PublishPolicies
    {
        public static PublishPolicy Default => new PriceCheck();
        
        private class PriceCheck : PublishPolicy
        {
            public Result Publish(Movie movie)
                => movie.PricingProfiles.Any() ? Result.Success() : Result.Error(new PriceCheckPolicyFailure());
        }
    }
}