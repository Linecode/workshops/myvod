using Linecode.DDD;
using MediatR;

namespace MyVod.Domain.Movies.Commands;

public record AddPriceProfileCommand(MovieId MovieId, PricingProfileCode PricingProfileCode) : IRequest<Result>;