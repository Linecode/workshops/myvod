using Linecode.DDD;
using MediatR;
using MyVod.Domain.Shared;

namespace MyVod.Domain.Movies.Commands;

public record CheckIfUserCanBuyAMovieCommand(MovieId MovieId, User User) : IRequest<Result<bool>>;