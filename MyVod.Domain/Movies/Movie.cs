using System;
using System.Collections.Generic;
using Linecode.DDD;
using Linecode.DDD.Extensions;
using MyVod.Domain.Movies.Events;
using MyVod.Domain.Movies.Policies;
using MyVod.Domain.Movies.Policies.Buy;
using MyVod.Domain.Movies.Policies.Publish;
using MyVod.Domain.Shared;

namespace MyVod.Domain.Movies
{
    public sealed class Movie : AggregateRoot<MovieId>
    {
        public string Title { get; private set; }
        public string Description { get; private set; }
        private readonly List<CastMember> _cast = new();
        public IReadOnlyCollection<CastMember> Cast => _cast;
        public MovieStatus Status { get; private set; }

        private readonly List<PricingProfile> _pricingProfiles = new();
        public IReadOnlyCollection<PricingProfile> PricingProfiles => _pricingProfiles;
        
        public byte[] Timestamp { get; private set; }

        public int Version { get; private set; } = 0;

        public bool IsPublished => Status == MovieStatus.Published;

        internal Movie()
        {
            Id = MovieId.New();
        }

        internal Movie(MovieId movieId)
        {
            Id = movieId;
        }

        public Movie(string title, string description)
        {
            Title = title;
            Description = description;

            Status = MovieStatus.Created;
            Id = MovieId.New();

            Raise(new MovieCreated(this));
        }

        internal void Apply(Events.AddCastMemberEvent @event)
        {
            _cast.Add(@event.CastMember);
            Version += 1;
        }

        internal void Apply(Events.DefineCastEvent @event)
        {
            _cast.RemoveAll(x => true);
            _cast.AddRange(@event.CastMembers);
            Version += 1;
        }

        internal void Apply(Events.PublishEvent @event)
        {
            var result = @event.Policy.Publish(this);

            if (result.IsSuccessful)
            {
                Status = MovieStatus.Published;
                Raise(new MoviePublished(this));
                Version += 1;
            }
            else
            {
                throw result.Exception;
            }
        }

        internal void Apply(Events.AddPricingProfile @event)
        {
            if (_pricingProfiles.NotContains(@event.PricingProfile))
            {
                _pricingProfiles.Add(@event.PricingProfile);
                Version += 1;
            }
        }

        internal void Apply(Events.CreateMovie @event)
        {
            var (title, description) = @event;
            Title = title;
            Description = description;
            Version += 1;
        }

        internal Result CanUserBuy(User user, BuyPolicy policy)
            => policy.Buy(this, user);

        internal static class Events
        {
            internal record CreateMovie(string Title, string Description) : IEvent;
            internal record AddCastMemberEvent(CastMember CastMember) : IEvent;

            internal record DefineCastEvent(ICollection<CastMember> CastMembers) : IEvent;

            internal record PublishEvent(PublishPolicy Policy) : IEvent;

            internal record AddPricingProfile(PricingProfile PricingProfile) : IEvent;
        }

        public enum MovieStatus
        {
            Created,
            Published
        }
    }
}