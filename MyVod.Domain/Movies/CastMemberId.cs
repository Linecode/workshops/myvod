using System;
using System.ComponentModel;
using System.Globalization;

namespace MyVod.Domain.Movies
{
    [TypeConverter(typeof(CastMemberIdTypeConverter))]
    public readonly struct CastMemberId : IComparable<CastMemberId>, IEquatable<CastMemberId>
    {
        public Guid Value { get; }
        
        public static CastMemberId New() => new CastMemberId(Guid.NewGuid());

        public CastMemberId(Guid value) => Value = value;

        public bool Equals(CastMemberId other) => Value.Equals(other.Value);
        public override bool Equals(object obj) => obj is CastMemberId other && Equals(other);
        public int CompareTo(CastMemberId other) => Value.CompareTo(other.Value);
        public override int GetHashCode() => Value.GetHashCode();
        public override string ToString() => $"CastMemberId: {Value.ToString()}";
        public static bool operator ==(CastMemberId a, CastMemberId b) => a.CompareTo(b) == 0;
        public static bool operator !=(CastMemberId a, CastMemberId b) => !(a == b);

        private class CastMemberIdTypeConverter : TypeConverter
        {
            public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
            {
                return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
            }

            public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
            {
                var stringValue = value as string;
                if (!string.IsNullOrEmpty(stringValue)
                    && Guid.TryParse(stringValue, out var guid))
                {
                    return new CastMemberId(guid);
                }

                return base.ConvertFrom(context, culture, value);
            }
        }
    }
}