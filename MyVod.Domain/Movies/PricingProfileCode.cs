using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using Linecode.DDD;
using Linecode.DDD.Extensions;
using Newtonsoft.Json;

namespace MyVod.Domain.Movies
{
    
    [TypeConverter(typeof(PriceProfileCodeTypeConverter))]
    [JsonConverter(typeof(PriceProfileCodeJsonConverter))]
    public sealed class PricingProfileCode : ValueObject<PricingProfileCode>
    {
        private static readonly IEnumerable<string> Codes = new[] {"AA", "BB", "CC"};
        
        public string Value { get; private set; }

        public PricingProfileCode(string value)
        {
            if (Codes.NotContains(value.ToUpperInvariant()))
                throw new ArgumentOutOfRangeException();
            
            Value = value.ToUpperInvariant();
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Value;
        }

        public static PricingProfileCode AA => new ("AA");
        public static PricingProfileCode BB => new ("BB");
        public static PricingProfileCode CC => new ("CC");
        
        private class PriceProfileCodeTypeConverter : TypeConverter
        {
            public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
            {
                return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
            }

            public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
            {
                var stringValue = value as string;
                if (!string.IsNullOrEmpty(stringValue))
                {
                    return new PricingProfileCode(stringValue);
                }

                return base.ConvertFrom(context, culture, value);
            }
        }

        private class PriceProfileCodeJsonConverter : JsonConverter
        {
            public override bool CanConvert(Type objectType)
            {
                return objectType == typeof(MovieId);
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                var id = (PricingProfileCode) value;
                serializer.Serialize(writer, id.Value);
            }

            public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
                JsonSerializer serializer)
            {
                var code = serializer.Deserialize<string>(reader);
                return new PricingProfileCode(code);
            }
        }
    }
}