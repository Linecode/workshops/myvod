using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("MyVod.Domain.Tests")]
[assembly: InternalsVisibleTo("MyVod.Infrastructure")]
[assembly: InternalsVisibleTo("MyVod.IntegrationTests")]