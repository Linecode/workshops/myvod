using FluentAssertions;
using MyVod.Domain.Movies;

namespace MyVod.Domain.Tests.Movies.Helpers
{
    internal class MovieAssert
    {
        private readonly Movie _movie;

        public MovieAssert(Movie movie)
        {
            _movie = movie;
        }

        public MovieAssert HaveTitleAndDescription(string title, string description)
        {
            _movie.Title.Should().Be(title);
            _movie.Description.Should().Be(description);
            return this;
        }

        public MovieAssert BePublished()
        {
            _movie.Status.Should().Be(Movie.MovieStatus.Published);
            return this;
        }

        public MovieAssert RaiseDomainEvent<T>()
        {
            _movie.DomainEvents.Should().ContainItemsAssignableTo<T>();
            return this;
        }
    }
}