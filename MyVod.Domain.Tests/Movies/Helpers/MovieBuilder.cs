using MyVod.Domain.Movies;
using MyVod.Domain.Movies.Policies.Publish;

namespace MyVod.Domain.Tests.Movies.Helpers
{
    internal class MovieBuilder
    {
        private Movie _movie;

        public MovieBuilder(string title = "Die Hard", string description = "Great Movie 3/5")
        {
            _movie = new Movie(title, description);
        }

        internal MovieBuilder WithPricingProfile(PricingProfile pricingProfile)
        {
            _movie.Apply(new Movie.Events.AddPricingProfile(pricingProfile));
            return this;
        }

        internal MovieBuilder IsPublished()
        {
            _movie.Apply(new Movie.Events.PublishEvent(PublishPolicies.Default));
            return this;
        }

        internal Movie Build() => _movie;
    }
}