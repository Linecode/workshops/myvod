using FluentAssertions;
using MyVod.Domain.Movies;
using MyVod.Domain.Movies.Events;
using MyVod.Domain.Movies.Policies;
using MyVod.Domain.Movies.Policies.Buy;
using MyVod.Domain.Movies.Policies.Publish;
using MyVod.Domain.Shared;
using MyVod.Domain.Tests.Movies.Helpers;
using Xunit;

namespace MyVod.Domain.Tests.Movies
{
    public abstract class MovieTests
    {
        public class WhenCreating : MovieTests
        {
            [Fact]
            public void It_Should_Set_Title_And_Description()
            {
                var title = "Die Hard";
                var description = "Great Movie 3/5";
                
                var movie = new Movie(title, description);

                var movieShould = new MovieAssert(movie);
                movieShould.HaveTitleAndDescription(title, description);
            }

            [Fact]
            public void It_Should_Publish_MovieCreated_Event()
            {
                var title = "Die Hard";
                var description = "Great Movie 3/5";
                
                var movie = new Movie(title, description);

                var movieShould = new MovieAssert(movie);
                movieShould.RaiseDomainEvent<MovieCreated>();
            }
        }
        
        public class WhenPublishing : MovieTests
        {
            [Fact]
            public void It_Should_Publish_DomainEvent_MoviePublished()
            {
                var movie = new MovieBuilder()
                    .WithPricingProfile(PricingProfile.GetBasedOnCode(PricingProfileCode.AA))
                    .Build();
                var publishEvent = new Movie.Events.PublishEvent(PublishPolicies.Default);

                movie.Apply(publishEvent);

                var movieShould = new MovieAssert(movie);

                movieShould
                    .BePublished();
            }

            [Fact]
            public void It_Should_Throw_Exception_If_Publish_Is_Called_When_Movie_Is_Not_Ready_To_Be_Published()
            {
                var movie = new MovieBuilder().Build();
                var publishEvent = new Movie.Events.PublishEvent(PublishPolicies.Default);

                Assert.Throws<PriceCheckPolicyFailure>(() =>
                {
                    movie.Apply(publishEvent);
                });
            }
        }

        public class WhenTryingToBuy : MovieTests
        {
            [Fact]
            public void It_Should_Succeed_If_User_Has_Pricing_Profile()
            {
                var user = new User
                {
                    Groups = new[] { "AA" }
                };
                var policy = BuyPolicies.Default;
                var movie = new MovieBuilder()
                    .WithPricingProfile(PricingProfile.GetBasedOnCode(PricingProfileCode.AA))
                    .IsPublished()
                    .Build();

                var result = movie.CanUserBuy(user, policy);

                result.IsSuccessful.Should().BeTrue();
            }

            [Fact]
            public void It_Should_Fail_If_User_Is_Do_Not_Have_Pricing_Profile()
            {
                var user = new User();
                var policy = BuyPolicies.Default;
                var movie = new MovieBuilder()
                    .WithPricingProfile(PricingProfile.GetBasedOnCode(PricingProfileCode.AA))
                    .IsPublished()
                    .Build();

                var result = movie.CanUserBuy(user, policy);

                result.IsSuccessful.Should().BeFalse();
            }

            [Fact]
            public void It_Should_Fail_If_Movie_Is_Not_Published()
            {
                var user = new User
                {
                    Groups = new[] { "AA" }
                };
                var policy = BuyPolicies.Default;
                var movie = new MovieBuilder()
                    .WithPricingProfile(PricingProfile.GetBasedOnCode(PricingProfileCode.AA))
                    .Build();

                var result = movie.CanUserBuy(user, policy);

                result.IsSuccessful.Should().BeFalse();
            }
        }
    }
}