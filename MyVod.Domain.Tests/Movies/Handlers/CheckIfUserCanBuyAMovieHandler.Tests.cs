using System.Threading;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Linecode.DDD.Exceptions;
using Moq;
using MyVod.Domain.Movies;
using MyVod.Domain.Movies.Commands;
using MyVod.Domain.Movies.Handlers;
using MyVod.Domain.Movies.Repositories;
using MyVod.Domain.Shared;
using MyVod.Domain.Tests.Movies.Helpers;
using Xunit;

namespace MyVod.Domain.Tests.Movies.Handlers
{
    public abstract class CheckIfUserCanBuyAMovieHandlerTests
    {
        public class WhenHandling : CheckIfUserCanBuyAMovieHandlerTests
        {
            private readonly IFixture _fixture;
            private IMoviesRepository _repository;
            private CheckIfUserCanBuyAMovieHandler _sut;

            public WhenHandling()
            {
                _fixture = new Fixture().Customize(new AutoMoqCustomization());
                _repository = _fixture.Freeze<IMoviesRepository>();
                _sut = _fixture.Create<CheckIfUserCanBuyAMovieHandler>();
            }

            [Fact]
            public async Task It_Should_Returns_Error_Result_If_Movie_Is_Not_Found()
            {
                Mock.Get(_repository).Setup(x => x.Get(It.IsAny<MovieId>()))
                    .ReturnsAsync((Movie) null);
                var command = new CheckIfUserCanBuyAMovieCommand(MovieId.New(), new User());

                var result = await _sut.Handle(command, CancellationToken.None);

                result.IsSuccessful.Should().BeFalse();
                result.Exception.Should().BeOfType<NotFoundException>();
            }

            [Fact]
            public async Task It_Should_Allow_User_To_Buy_A_Movie_When_User_Has_Pricing_Profile()
            {
                var movie = new MovieBuilder()
                    .WithPricingProfile(PricingProfile.GetBasedOnCode(PricingProfileCode.AA))
                    .IsPublished()
                    .Build();
                var user = new User {Groups = new []{ "AA" }};
                Mock.Get(_repository).Setup(x => x.Get(It.IsAny<MovieId>()))
                    .ReturnsAsync(movie);
                var command = new CheckIfUserCanBuyAMovieCommand(MovieId.New(), user);
                
                var result = await _sut.Handle(command, CancellationToken.None);

                result.IsSuccessful.Should().BeTrue();
                result.Data.Should().BeTrue();
            }

            [Fact]
            public async Task It_Should_Disallow_User_To_Buy_A_Movie_When_User_Does_Not_Have_Pricing_Profile_Group()
            {
                var movie = new MovieBuilder()
                    .WithPricingProfile(PricingProfile.GetBasedOnCode(PricingProfileCode.AA))
                    .IsPublished()
                    .Build();
                var user = new User {Groups = new []{ "" }};
                Mock.Get(_repository).Setup(x => x.Get(It.IsAny<MovieId>()))
                    .ReturnsAsync(movie);
                var command = new CheckIfUserCanBuyAMovieCommand(MovieId.New(), user);
                
                var result = await _sut.Handle(command, CancellationToken.None);

                result.IsSuccessful.Should().BeTrue();
                result.Data.Should().BeFalse();
            }

            [Fact]
            public async Task It_Should_Allow_To_Buy_For_Vip_User_Even_If_Movie_Is_Unpublished()
            {
                var movie = new MovieBuilder()
                    .Build();
                
                var user = new User {Groups = new []{ "Vip" }};
                Mock.Get(_repository).Setup(x => x.Get(It.IsAny<MovieId>()))
                    .ReturnsAsync(movie);
                var command = new CheckIfUserCanBuyAMovieCommand(MovieId.New(), user);
                
                var result = await _sut.Handle(command, CancellationToken.None);

                result.IsSuccessful.Should().BeTrue();
                result.Data.Should().BeTrue();
            }
        }
    }
}