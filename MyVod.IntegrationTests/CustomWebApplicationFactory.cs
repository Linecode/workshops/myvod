using System;
using System.Linq;
using System.Net.Http.Json;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MyVod.Domain.Movies;
using MyVod.Domain.Movies.Events;
using MyVod.Infrastructure.Movies;
using Newtonsoft.Json;

namespace MyVod.IntegrationTests
{
    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<TStartup> where TStartup : class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                var descriptor =
                    services.SingleOrDefault(d => d.ServiceType == typeof(DbContextOptions<MoviesContext>));
                services.Remove(descriptor);

                services.AddDbContext<MoviesContext>(options =>
                {
                    InMemoryDbContextOptionsExtensions.UseInMemoryDatabase(options, "InMemory");
                });

                var sp = services.BuildServiceProvider();
                using var scope = sp.CreateScope();
                var db = scope.ServiceProvider.GetRequiredService<MoviesContext>();

                db.Database.EnsureCreated();
                
                InitializeDatabase(db);
            });
        }

        private void InitializeDatabase(MoviesContext context)
        {
            var guid = Guid.Parse("0bcfb025-2249-46ef-a9cd-d4be60d63d57");
            context.Events.Add(new Event(DateTime.Now,
                JsonConvert.SerializeObject(new { title = "Die Hard", description = "Lorem" }),
                guid, typeof(Movie.Events.CreateMovie).ToString(), 0
            ));

            var id = new MovieId(guid);
            var movie = new Movie(id);
            movie.Apply(new Movie.Events.CreateMovie("Lion King", "Lorem"));
            context.Movies.Add(movie);

            context.SaveChanges();
        }
    }
    
    
}