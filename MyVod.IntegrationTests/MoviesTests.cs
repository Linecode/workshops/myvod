using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using MyVod.Api;
using MyVod.Api.Movies.Resources;
using MyVod.Infrastructure.Movies;
using Newtonsoft.Json;
using Xunit;

namespace MyVod.IntegrationTests;

public class MoviesTests : IClassFixture<CustomWebApplicationFactory<Startup>>
{
    private readonly CustomWebApplicationFactory<Startup> _factory;
    private readonly HttpClient _httpClient;

    public MoviesTests(CustomWebApplicationFactory<Startup> factory)
    {
        _factory = factory;
        _httpClient = factory.CreateDefaultClient();
    }

    [Fact]
    public async Task Movies_Create()
    {
        var content = JsonContent.Create(new { title = "Die Hard", description = "Lorem Lipsum" });

        var response = await _httpClient.PostAsync("/api/movies", content);

        response.EnsureSuccessStatusCode();

        using var scope =_factory.Services.CreateScope();
        var context = scope.ServiceProvider.GetRequiredService<MoviesContext>();
        var result = context.Movies.AsQueryable().First(x => x.Title.Equals("Die Hard"));
        result.Should().NotBeNull();
    }

    [Fact]
    public async Task Movie_Create_MediaType()
    {
        var content = JsonContent.Create(new { title = "Die Hard", description = "Lorem Lipsum" });

        var response = await _httpClient.PostAsync("/api/movies", content);

        response.Content.Headers.ContentType?.MediaType.Should().Be("application/json");
    }

    [Fact]
    public async Task Movie_CreatedHeader()
    {
        var content = JsonContent.Create(new { title = "Die Hard", description = "Lorem Lipsum" });

        var response = await _httpClient.PostAsync("/api/movies", content);

        response.Headers.Location?.Should().NotBeNull();
        response.Headers.Location?.ToString().Should().Contain("/api/movies");
    }

    [Fact]
    public async Task Movie_Get()
    {
        var response = await _httpClient.GetStringAsync("/api/movies/0bcfb025-2249-46ef-a9cd-d4be60d63d57");

        var body = JsonConvert.DeserializeObject<MovieResource>(response);

        body.Title.Should().Be("Lion King");
    }
}
