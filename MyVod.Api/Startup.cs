using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using MyVod.Api.Utils;
using MyVod.Domain.Movies;
using MyVod.Domain.Movies.Repositories;
using MyVod.Infrastructure.Movies;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace MyVod.Api;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    private IConfiguration Configuration { get; }

    public void ConfigureServices(IServiceCollection services)
    {
        services.AddDbContext<MoviesContext>(options =>
        {
            options.UseSqlServer("Server=localhost,1433;Database=MyVodV2;User Id=SA;Password=yourStrong(!)Password");
        });

        services.AddTransient<IMoviesRepository, MoviesRepository>();
        services.AddTransient<IEventStore, MovieEventStore>();
            
        services.AddMediatR(typeof(Movie).Assembly);

        services.AddHostedService<OutboxHostedService>();
            
        services.AddControllers(options =>
            {
                options.Conventions.Add(new RouteTokenTransformerConvention(new SlugifyParameterTransformer()));
            })
            .AddNewtonsoftJson(x =>
            {
                x.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                x.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    
                x.SerializerSettings.Converters.Add(new StringEnumConverter());
            });
            
        services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1", new OpenApiInfo { Title = "MyVod.Api", Version = "v1" });
            c.MapType<MovieId>(() => new OpenApiSchema
            {
                Type = "string",
                Example = new OpenApiString("GUID")
            });
            c.MapType<PricingProfileCode>(() => new OpenApiSchema
            {
                Type = "string",
                Example = new OpenApiString("AA")
            });
        });


        // #region RecreateDatabase
        //
        // using var provider = services.BuildServiceProvider();
        // using var context = provider.GetService<MoviesContext>();
        //
        // context.Database.EnsureDeleted();
        // context.Database.EnsureCreated();
        //
        // #endregion
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "MyVod.Api"));
        }

        app.UseHttpsRedirection();

        app.UseRouting();

        app.UseAuthorization();

        app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
    }
}