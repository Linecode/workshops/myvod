using System;
using System.Threading.Tasks;
using Linecode.DDD.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyVod.Api.Movies.Resources;
using MyVod.Domain.Movies;
using MyVod.Domain.Movies.Commands;
using MyVod.Domain.Movies.Policies.Publish;
using MyVod.Domain.Shared;

namespace MyVod.Api.Movies;

[ApiController]
[Route("api/[controller]")]
public class MoviesController : ControllerBase
{
    private readonly ISender _sender;

    public MoviesController(ISender sender)
    {
        _sender = sender;
    }

    [HttpPost]
    public async Task<IActionResult> Create(CreateMovieResource resource)
    {
        var result = await _sender.Send(new CreateMovieCommand(resource.Title, resource.Description));

        return result.Match<IActionResult>(
            success: data => Created(new Uri($"/api/movies/{data.Value}", UriKind.Relative), null),
            error: error => error switch
            {
                UniqueConstrainViolationException => Conflict(),
                _ => StatusCode(StatusCodes.Status500InternalServerError, error)
            });
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> Get(MovieId id)
    {
        var movie = await _sender.Send(new GetMovieCommand(id));

        return movie.Match<IActionResult>(
            success: data => Ok(MovieResource.From(data)), 
            error: exception => exception switch
            {
                NotFoundException => NotFound(),
                _ => StatusCode(StatusCodes.Status500InternalServerError, exception)
            });
    }

    [HttpPost("{id}/[action]")]
    public async Task<IActionResult> SetPriceProfile(MovieId id, [FromBody] PricingProfileCode pricingProfileCode)
    {
        var result = await _sender.Send(new AddPriceProfileCommand(id, pricingProfileCode));

        return result.Match<IActionResult>(
            success: Ok, 
            error => BadRequest());
    }

    [HttpPost("{id}/[action]")]
    public async Task<IActionResult> Publish(MovieId id)
    {
        var result = await _sender.Send(new PublishMovieCommand(id));

        return result.Match<IActionResult>(success: Ok, exception => exception switch
        {
            NotFoundException => NotFound(),
            PriceCheckPolicyFailure => StatusCode(StatusCodes.Status412PreconditionFailed, new { info = "Missing Price" }),
            _ => StatusCode(StatusCodes.Status500InternalServerError)
        });
    }

    [HttpPost("{id}/[action]")]
    public async Task<IActionResult> CanBuy(MovieId id, [FromBody] User user)
    {
        var result = await _sender.Send(new CheckIfUserCanBuyAMovieCommand(id, user));

        return result.Match<IActionResult>(
            success: data => Ok(data),
            error: error => BadRequest());
    }
}
