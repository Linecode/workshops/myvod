using System.Collections.Generic;
using System.Linq;
using MyVod.Domain.Movies;
using MyVod.Domain.Shared;

namespace MyVod.Api.Movies.Resources;

public class CreateMovieResource
{
    public string Title { get; set; }
    public string Description { get; set; }
}

public class MovieResource : CreateMovieResource
{
    public MovieId Id { get; set; }
    public List<PricingProfile> PricingProfiles { get; set; }
        
    public static MovieResource From(Movie movie)
        => new()
        {
            Id = movie.Id,
            Description = movie.Description,
            Title = movie.Title,
            PricingProfiles = movie.PricingProfiles.ToList()
        };
}
