using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using MyVod.Domain.Movies;
using MyVod.Domain.Shared;

namespace MyVod.Infrastructure.Movies;

public class MovieEntityTypeConfiguration : IEntityTypeConfiguration<Movie>
{
    public void Configure(EntityTypeBuilder<Movie> builder)
    {
        builder.HasKey(x => x.Id);
            
        builder.Property(x => x.Id)
            .HasConversion(id => id.Value,
                s => new MovieId(s));

        builder.Ignore(x => x.DomainEvents);

        builder.Property(x => x.Title);
        builder.HasIndex(x => x.Title)
            .IsUnique();
            
        builder.Property(x => x.Description);
        builder.Property(x => x.Status)
            .HasConversion<string>();
        builder.Property(x => x.Timestamp)
            .IsRowVersion();

        builder.OwnsMany(x => x.PricingProfiles, p =>
        {
            p.HasKey("MovieId", "Code");

            p.Property(x => x.Code)
                .HasConversion(x => x.Value, s => new PricingProfileCode(s));

            p.OwnsOne(x => x.Price, y =>
            {
                y.Property(x => x.Currency)
                    .HasConversion<string>()
                    .HasColumnName("Price_Currency");
                y.Property(x => x.Value)
                    .HasColumnName("Price_Value")
                    .HasPrecision(10, 2);
            });
                
            p.ToTable("MoviesPricingProfiles", MoviesContext.Schema);
        });

        builder.HasMany(x => x.Cast);

        builder.ToTable("Movies", MoviesContext.Schema);
    }
}