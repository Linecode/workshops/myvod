using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyVod.Domain.Movies;
using MyVod.Domain.Movies.Repositories;
using Newtonsoft.Json;

namespace MyVod.Infrastructure.Movies;

internal class Event
{
    public int Id { get; private set; }
    public DateTime OccuredOn { get; private set; }
    public string Payload { get; private set; }
    public string Type { get; private set; }
    public Guid AggregateId { get; private set; }
    public int Version { get; private set; }

    public Event(DateTime occuredOn, string payload, Guid aggregateId, string type, int version)
    {
        OccuredOn = occuredOn;
        Payload = payload;
        AggregateId = aggregateId;
        Type = type;
        Version = version;
    }
}

internal class EventTypeConfiguration : IEntityTypeConfiguration<Event>
{
    public void Configure(EntityTypeBuilder<Event> builder)
    {
        builder.ToTable("MovieEvents", MoviesContext.Schema);

        builder.HasKey(x => x.Id);

        builder.HasIndex(x => new {x.AggregateId, x.Version})
            .IsUnique();

        builder.HasIndex(x => x.AggregateId)
            .IsClustered(false);

        builder.Property(x => x.Version)
            .IsConcurrencyToken();
    }
}
    
public class MovieEventStore : IEventStore
{
    private readonly MoviesContext _context;

    public MovieEventStore(MoviesContext context)
    {
        _context = context;
    }

    public async Task Add(Guid aggregateId, object eventPayload, int version)
    {
        var type = eventPayload.GetType().ToString();
        var @event = new Event(DateTime.UtcNow,
            JsonConvert.SerializeObject(eventPayload,
                new JsonSerializerSettings {ReferenceLoopHandling = ReferenceLoopHandling.Ignore}),
            aggregateId,
            type,
            version);

        _context.Events.Add(@event);
        await _context.SaveChangesAsync();
    }

    public async Task<Movie> Get(Guid aggregateId)
    {
        var stream = await _context.Events.Where(x => x.AggregateId == aggregateId)
            .OrderBy(x => x.OccuredOn)
            .ToListAsync();

        var events = stream.Select(x =>
        {
            var type = Assembly.GetAssembly(typeof(Movie)).GetType(x.Type);
            var obj = JsonConvert.DeserializeObject(x.Payload, type);
            return (dynamic) obj;
        }).ToList();
            
        var movie = new Movie(new MovieId(aggregateId));

        foreach (var @event in events)
        {
            movie.Apply(@event);
        }

        return movie;
    }
}