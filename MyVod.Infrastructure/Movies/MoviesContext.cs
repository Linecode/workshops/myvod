using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Linecode.DDD;
using Linecode.DDD.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyVod.Domain.Movies;
using Newtonsoft.Json;

namespace MyVod.Infrastructure.Movies;

public class MoviesContext : DbContext, IUnitOfWork
{
    private readonly IMediator _mediator;
        
    public DbSet<Movie> Movies { get; set; }
    public DbSet<OutboxMessage> Outbox { get; set; }
        
    internal DbSet<Event> Events { get; set; }

    public MoviesContext(DbContextOptions<MoviesContext> options, IMediator mediator) : base(options)
    {
        _mediator = mediator;
    }
        
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    { 
        // optionsBuilder.UseSqlServer(@"Server=localhost,1433;Database=MyVod;User Id=SA;Password=yourStrong(!)Password");
        optionsBuilder.UseLoggerFactory(MyLoggerFactory);
    }

    public virtual async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
    {
        // await _mediator.DispatchDomainEventsAsync<MoviesContext>(this).ConfigureAwait(false);

        #region outbox

        var domainEntities = ChangeTracker
            .Entries<IEntity>()
            .Where(x => x.Entity.DomainEvents != null && x.Entity.DomainEvents.Any());
            
        var domainEvents = domainEntities
            .SelectMany(x => x.Entity.DomainEvents)
            .ToList();
            
        foreach (var domainEvent in domainEvents)
        {
            var type = domainEvent.GetType().FullName;
            var data = JsonConvert.SerializeObject(domainEvent, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore});
            var outboxMessage = new OutboxMessage(
                DateTime.UtcNow, 
                type,
                data);
            
            Outbox.Add(outboxMessage);
        }

        #endregion

        try
        {
            await base.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
            return true;

        }
        catch (Exception e)
        {
            static bool IsConstrainViolation(Exception e, string constrainName)
                => e.InnerException != null && e.InnerException.Message.Contains(constrainName);
                
            if (IsConstrainViolation(e, "IX_Movies_Title"))
                throw new UniqueConstrainViolationException();

            // Other exceptions goes here
            throw;
        }
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(MoviesContext).Assembly);
            
        base.OnModelCreating(modelBuilder);
    }

    public static string Schema = "movies";
        
    static readonly ILoggerFactory MyLoggerFactory = LoggerFactory.Create(builder => { builder.AddConsole(); });
}